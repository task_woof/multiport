# Multiport

```bash
$ curl -X POST -F "part=@test.jpg"  http://localhost:8080/multipart/form/gray -o tmp.jpeg
```

```
$ docker build --build-arg WAR_FILE=./target/multipart.war -t multipart .
```

```
$ docker run -d -p 8080:8080 multipart
```

```
$ curl -X POST -F "part=@lena.png" http://localhost:8080/app/form/gray -o gray.png
$ open gray.png
```
