package com.example.multipart;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;

/**
 * Root resource (exposed at "myresource" path)
 */
@Path("/form")
public class MyResource {
	
	private static final String UPLOAD_PATH = "/tmp";
	
	@POST
	@Path("part")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String post(@FormDataParam("part") String s) {
		return s;
	}
	
	@POST
	@Path("part-file-name")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public String post(
			@FormDataParam("part") String s,
			@FormDataParam("part") FormDataContentDisposition d) {
		String fileName = d.getFileName();
		System.out.println("check: " + fileName);
		
		// return s + ":" + d.getFileName();
		return d.getFileName();
	}

	/**
	 * 
	 * @param is
	 * @param d
	 * @return
	 */
	@POST
	@Path("gray")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response post(
			@FormDataParam("part") InputStream is,
			@FormDataParam("part") FormDataContentDisposition d) {
		String fileName = d.getFileName();
		
		// save file
		saveFile(is, fileName);
		
		// rgb -> gray
	    toGray(fileName);
		
		File file = new File (UPLOAD_PATH + File.separator + "gray-" + fileName);
		ResponseBuilder rb = Response.ok((Object) file);
		rb.header("Content-Disposition", "attachment; filename=\"temp.jpeg\"");

		return rb.build();
	}

	/**
	 * Save InputStrem data into specific file.
	 * @param is
	 * @param fileName
	 */
	private void saveFile(InputStream is, String fileName) {
		java.nio.file.Path path = FileSystems.getDefault().getPath(UPLOAD_PATH + File.separator + fileName);
		try {
			Files.copy(is, path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * This method make input file grascale using OpenCV.
	 * 
	 * @param filename File name
	 */
	private void toGray(String filename) {
		Mat source = imread(UPLOAD_PATH + File.separator + filename);
		Mat dest = new Mat(source.size(), CV_8UC1);
		
		// convert RGB -> Gray
		cvtColor(source, dest, COLOR_BGR2GRAY);
		
		// save procesed image
		imwrite(UPLOAD_PATH + File.separator + "gray-" + filename, dest);
	}
}
